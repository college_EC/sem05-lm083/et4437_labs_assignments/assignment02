/*
 This file implements a DataRecord object to save data from the GUI.
*/

package com.ET4437.Assignment02;

public class DataRecord implements GUIRecord {

    //instance variables
    private String name;
    private String username;
    private String password;
    private String gender;
    private String occupation;
    private String comment;
    private boolean subscribed;

    /*
     * Constructor Methods.
     * Below are 2 constructor methods, one with parameters and one without parameters.
     * The one with parameters was used in testing but will remain throughout the assignment
     * for completion sake.
     */
    // Initializes the DataRecord object with default values.
    public DataRecord(){
        this.name = "";
        this.username = "";
        this.password = "";
        this.gender = "";
        this.occupation = "";
        this.comment = "";
        this.subscribed = false;
    }

    // Initialises the DataRecord object with values provided
    public DataRecord(String name, String username, String password, String gender, String occupation, String comment, boolean subscribed){
        this.name = name;
        this.username = username;
        this.password = password;
        this.gender = gender;
        this.occupation = occupation;
        this.comment = comment;
        this.subscribed = subscribed;
    }

    /*
     * Getter functions
     * These functions return the instance variables that are in the object.
     */
    public String getName() {
        return this.name;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getGender() {
        return this.gender;
    }

    public String getOccupation() {
        return this.occupation;
    }

    public String getComment() {
        return this.comment;
    }

    public boolean getSubscribed() {
        return this.subscribed;
    }

    /*
     * Setter functions
     * Sets the instance variables to be the passed in value
     */

    public void setName(String Name) {
        this.name = Name;
    }

    public void setUsername(String Username) {
        this.username = Username;
    }

    public void setPassword(String Password) {
        this.password = Password;
    }

    public void setGender(String Gender) {
        this.gender = Gender;
    }

    public void setOccupation(String Occupation) {
        this.occupation = Occupation;
    }

    public void setComment(String Comment) {
        this.comment = Comment;
    }

    public void setSubscribed(boolean Subscribed) {
        this.subscribed = Subscribed;
    }

    public static void main(String[] args) {
        // Creating test 01
        DataRecord testSetter = new DataRecord();
        // Setting data to the above object
        testSetter.setName("Eoghan Conlon");
        testSetter.setUsername("eoghanconlon73");
        testSetter.setPassword("Moneygall");
        testSetter.setGender("Male");
        testSetter.setOccupation("Student");
        testSetter.setComment("This is a test for the DataRecord object");
        testSetter.setSubscribed(true);
        // Getting that data
        System.out.printf("Name: %s \n Username: %s \n Password: %s \n Gender: %s \n Occupation: %s \n Comment: %s \n Subscribed: %b \n",
                testSetter.getName(), testSetter.getUsername(), testSetter.getPassword(), testSetter.getGender(), testSetter.getOccupation(),
                testSetter.getComment(), testSetter.getSubscribed());
        // Testing the full object creator
        DataRecord testInit = new DataRecord("John", "j_doe", "UL", "non-binary",
                "Student", "This is testing the initilaisation", true);
        // Testing to see if it actually made it
        System.out.printf("Name: %s \n Username: %s \n Password: %s \n Gender: %s \n Occupation: %s \n Comment: %s \n Subscribed: %b \n",
                testInit.getName(), testInit.getUsername(), testInit.getPassword(), testInit.getGender(), testInit.getOccupation(),
                testInit.getComment(), testInit.getSubscribed());
    }
}
