/* 
   This is a Swing GUI example that implements a simple form
*/

package com.ET4437.Assignment02;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

// class declaration
public class SwingGUIExample extends JFrame {
    
    // define all instance variables as private
    private final JPanel panel;
    private final JButton submit;
    private final JTextField name;
    private final JTextField username;
    private final JPasswordField password;
    private final JRadioButton male;
    private final JRadioButton female;
    private final JRadioButton nonbinary;
    private final JComboBox occupationlist;
    private final ButtonGroup bg; 
    private final JTextArea comments;
    private final JLabel namelabel;
    private final JLabel userlabel;
    private final JLabel passwordlabel;  
    private final JLabel occupationlabel;
    private final JCheckBox subscribe;
    private ArrayList records;
    private byte[] hashpass;
     
    // No-parameter constructor
    SwingGUIExample() {
        // set window name
        super("Swing GUI Example");

        // occupation list
        String[] ocList = {"Student", "Lecturer", "Researcher", "Engineer", "Other"};
        
        // set the size of the window
        setSize(500,450);
        // create a new panel
        panel = new JPanel();
        // use the Spring layout
        SpringLayout layout = new SpringLayout(); 
        panel.setLayout(layout);
        
        // create a full name label
        namelabel = new JLabel("Name:");
        // add the label to the panel
        panel.add(namelabel); 
        // create a new text field
        name = new JTextField(30);
        name.setToolTipText("Type your name");
        // add Action Listener for the text field
        name.addActionListener(new TextFieldAL());
        // add the text field to the panel
        panel.add(name);
        // modify the location of the name label
        layout.putConstraint(SpringLayout.WEST, namelabel,6, SpringLayout.WEST, panel); 
        layout.putConstraint(SpringLayout.NORTH, namelabel,8, SpringLayout.NORTH, panel);
        // modify the location of the name text field
        layout.putConstraint(SpringLayout.WEST, name,70, SpringLayout.WEST, namelabel); 
        layout.putConstraint(SpringLayout.NORTH, name, 1, SpringLayout.NORTH, namelabel); 
        
        // add a username label
        userlabel = new JLabel("Username:");
        // add the text label to the panel
        panel.add(userlabel);
        // create a new text field
        username = new JTextField(30);
        username.setToolTipText("Type your username");
        // set Action Listener for the text field
        username.addActionListener(new TextFieldAL());
        // add a username to the panel
        panel.add(username);
        // modify the location of the username label
        layout.putConstraint(SpringLayout.WEST, userlabel,6, SpringLayout.WEST, panel); 
        layout.putConstraint(SpringLayout.NORTH, userlabel,8+30, SpringLayout.NORTH, panel);
        // modify the location of the username text field
        layout.putConstraint(SpringLayout.WEST, username,70, SpringLayout.WEST, userlabel); 
        layout.putConstraint(SpringLayout.NORTH, username, 1, SpringLayout.NORTH, userlabel);
        
        // add a password label
        passwordlabel = new JLabel("Password:");
        // add the password label to the panel
        panel.add(passwordlabel);      
        // add a passowrd field
        password = new JPasswordField(30);
        password.setToolTipText("Type your password");
        // set Action Listener for the password field
        password.addActionListener(new TextFieldAL());
        // add the password field to the panel
        panel.add(password);
        // modify the location of the password label
        layout.putConstraint(SpringLayout.WEST, passwordlabel,6, SpringLayout.WEST, panel); 
        layout.putConstraint(SpringLayout.NORTH, passwordlabel,8+60, SpringLayout.NORTH, panel);
        // modify the location of the password field
        layout.putConstraint(SpringLayout.WEST, password,70, SpringLayout.WEST, passwordlabel); 
        layout.putConstraint(SpringLayout.NORTH, password, 1, SpringLayout.NORTH, passwordlabel);   
        
        // add a gender radio button
        male = new JRadioButton("Male");
        // select the option
        male.setSelected(true); 
        // set Action Listener for the radio button
        male.addActionListener(new RadioButtonAL());
        // add another radio button
        female = new JRadioButton("Female");
        // use the same Action Listener
        female.addActionListener(new RadioButtonAL()); 
        // add another radio button
        nonbinary = new JRadioButton("Non-binary");
        // use the same Action Listener
        nonbinary.addActionListener(new RadioButtonAL());
        // create a new button group
        bg = new ButtonGroup(); 
        // add the two radio buttons to the same button group
        bg.add(male);
        bg.add(female);
        bg.add(nonbinary);
        // add the radio buttons to the panel
        panel.add(male);
        panel.add(female);
        panel.add(nonbinary);
        // modify the location of the "male" radio button
        layout.putConstraint(SpringLayout.WEST, male,70, SpringLayout.WEST, panel); 
        layout.putConstraint(SpringLayout.NORTH, male,90, SpringLayout.NORTH, panel);
        // modify the location of the "female" radio button
        layout.putConstraint(SpringLayout.WEST, female,70, SpringLayout.WEST, male); 
        layout.putConstraint(SpringLayout.NORTH, female, 0, SpringLayout.NORTH, male);
        // modify the location of the "non-binary" radio button
        layout.putConstraint(SpringLayout.WEST, nonbinary,80, SpringLayout.WEST, female); 
        layout.putConstraint(SpringLayout.NORTH, nonbinary, 0, SpringLayout.NORTH, female);
        
        // add an occupation label
        occupationlabel = new JLabel("Occupation:");
        // add the password label to the panel
        panel.add(occupationlabel);  
        // modify the location of the occupation label
        layout.putConstraint(SpringLayout.WEST, occupationlabel,6, SpringLayout.WEST, panel); 
        layout.putConstraint(SpringLayout.NORTH, occupationlabel,120, SpringLayout.NORTH, panel);
        // add an occupation list
        occupationlist = new JComboBox(ocList);
        // add a hover text
        occupationlist.setToolTipText("Select occupation");
        // add the password label to the panel
        panel.add(occupationlist);
        occupationlist.setSelectedIndex(0);
        // set an Action Listener for the combo box
        occupationlist.addActionListener(new ComboBoxAL());
        // modify the location of the occupation label
        layout.putConstraint(SpringLayout.WEST, occupationlist,80, SpringLayout.WEST, occupationlabel); 
        layout.putConstraint(SpringLayout.SOUTH, occupationlist,6, SpringLayout.SOUTH, occupationlabel);
        
        // add a new text area for comments
        comments = new JTextArea(8,30);
        comments.setToolTipText("Leave comments here ...");
        // add a scroll bar to the text area
        JScrollPane areaScrollPane = new JScrollPane(comments);
        // add the text area with the scroll bar to the panel
        panel.add(areaScrollPane);
        // modify the location of the text area
        layout.putConstraint(SpringLayout.WEST, areaScrollPane,75, SpringLayout.WEST, panel); 
        layout.putConstraint(SpringLayout.NORTH, areaScrollPane,160, SpringLayout.NORTH, panel);
        
        // add a check box for subscription
        subscribe = new JCheckBox("subscribe");
        // set Action Listener for the check box
        subscribe.addActionListener(new CheckBoxAL());
        // add the check box to the panel
        panel.add(subscribe);
        // modify the location of the "subscribe" check box
        layout.putConstraint(SpringLayout.WEST, subscribe,70, SpringLayout.WEST, panel); 
        layout.putConstraint(SpringLayout.NORTH, subscribe,325, SpringLayout.NORTH, panel);
        
        // add a push button "Submit"
        submit= new JButton("Submit");
        // add a hover text
        submit.setToolTipText("press button to submit");
        // set Action Listener for the push button
        submit.addActionListener(new ButtonAL());
        // add the button to the panel
        panel.add(submit);
        // modify the location of the push button
        layout.putConstraint(SpringLayout.WEST, submit,180, SpringLayout.WEST, panel); 
        layout.putConstraint(SpringLayout.NORTH, submit,350, SpringLayout.NORTH, panel);
        
         // add the panel to the window
        add(panel);
        // move to centre of screen
        setLocationRelativeTo(null);
        // make the window visible
        setVisible(true); 
        // set the default closing operation
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    // Action Listener class for the TextFields and PasswordField
    private class TextFieldAL implements ActionListener {
        // called when you press Enter in any of the text fields
        @Override
        public void actionPerformed(ActionEvent e) {
            // if Enter was pressed in the "name" text field ->
            if (e.getSource() == name) {
                // print the name
                System.out.println("Your name is: " + name.getText());
            }
            // if Enter was pressed in the "username" text field ->
            else if (e.getSource() == username) {
                // print the username
                System.out.println("Your username is: " + username.getText());
            }
            // if Enter was pressed in the "password" text field ->
            else if (e.getSource() == password) {
                // convert the password to a string
                String strpass = new String(password.getPassword());
                // print the password
                System.out.println("Your password is: " + strpass);
                try {
                    // encrypt the password using the hashing algorithm SHA-512
                    MessageDigest passdigest = MessageDigest.getInstance("SHA-512");
                    hashpass = passdigest.digest(strpass.getBytes(StandardCharsets.UTF_8));
                    // print the hashed password
                    System.out.print("Your hashed password is: ");
                    for (byte j : hashpass) 
                        System.out.printf("%02x", j);
                    System.out.println();
                } 
                catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
            // Uncrecognized text field
            else {
                System.out.println("Error: Textfield not found!");
            }     
        }    
    }
    
    // Action Listener class for the RadioButtons
    private class RadioButtonAL implements ActionListener {
        // called when you select a radio button
        @Override
        public void actionPerformed(ActionEvent e) {
            //put your code here: Example - male.isSelected();
            System.out.println(e.getActionCommand());
        }    
    }
    
    // Action Listener class for the CheckBox
    private class CheckBoxAL implements ActionListener {
        // called when you check/uncheck a check box
        @Override
        public void actionPerformed(ActionEvent e) {
            //put your code here: Example - subscribe.isSelected();
        }    
    }
    
    // Action Listener class for the Combo Box
    private class ComboBoxAL implements ActionListener {
        // called when you select an item
        @Override
        public void actionPerformed(ActionEvent e) {
            // put your code here: Example - prints the name of the selected item 
             System.out.println(occupationlist.getSelectedItem());
        }    
    }
    
    // Action Listener class for the Submit Button
    private class ButtonAL implements ActionListener {
        // called when you press a button
        @Override
        public void actionPerformed(ActionEvent e) {
            // put your code here  
        }    
    }
    
   // Main method 
    public static void main(String[] args) {
        // create a new object and initialize it
        SwingGUIExample ex = new SwingGUIExample(); 
    }
}