/* 
   This is an interface file and must not be modified.
   Implement this file as a JavaBean class file in the assignment
 */
package com.ET4437.Assignment02;

public interface GUIRecord {
    // returns the name
    public String getName();
    // returns the username
    public String getUsername();
    // this must return the hashed password
    public String getPassword();
    // this must return the gender
    public String getGender();
    // this must return the occupation
    public String getOccupation();
    // returns the comment
    public String getComment();
    // returns the subscription status: subscribed = true, unsubscribed = false 
    public boolean getSubscribed();
    // this method sets the name 
    public void setName(String Name);
    // this method sets the username
    public void setUsername(String Username);
    // this method must set the hashed password
    public void setPassword(String Password);
    // this method must set the gender
    public void setGender(String Gender);
    // this method must set the occupation
    public void setOccupation(String Gender);
    // this method sets the comment
    public void setComment(String Comment);
    // this method sets the subscription status
    public void setSubscribed(boolean Subscribed);
}
